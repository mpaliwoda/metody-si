#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <random>
#include <string>
#include <limits.h>
//***********************************************************************************************************
struct Item {
    unsigned short Weight;
    unsigned short Value;
};

#define MAX_KNAPSACK_WEIGHT 200u
#define NUMBER_OF_COMBINATIONS ( 2u * INT_MAX )
#define ELEMENTS_NUMBER ( sizeof( int ) * 8 )

std::random_device rd;
std::uniform_int_distribution<int> distribution( INT_MIN, INT_MAX );

#define CHECK_BIT(var, pos) ( (var) & ( 1 << ( pos ) ) )
#define CHANGE_BIT(var, pos) ( (var) ^ ( 1 << ( pos ) ) )
//***********************************************************************************************************
void SetBackpackParameters( Item* items, 
                          unsigned int backpack, 
                          unsigned int &weight, 
                          unsigned int &value) {

    for( int i = 0; i < ELEMENTS_NUMBER; i++ ) {
        if( !CHECK_BIT( backpack, i ) ) 
            continue;
        weight += items[i].Weight;
        value += items[i].Value;
    }
}
//***********************************************************************************************************
void PointAsStr( unsigned int point, std::string &strPoint ) {
    for( int i = 0; i < ELEMENTS_NUMBER; i++ ) {
        if( CHECK_BIT( point, i ) ) {
            strPoint += '1'; 
        } else {
            strPoint += '0';
        }
    }
}
//***********************************************************************************************************
void FullSearch( Item* items, 
                 unsigned int &globalMaximumValue, 
                 unsigned int &globalMaximum ) {
	
    for( int i = 0; i < NUMBER_OF_COMBINATIONS; i++ ) {
        unsigned int weight = 0; 
        unsigned int value = 0;
        SetBackpackParameters( items, i, weight, value );

        if( weight <= MAX_KNAPSACK_WEIGHT && value > globalMaximumValue ) {
            globalMaximumValue = value;
            globalMaximum = i;
        }
    }
}
//***********************************************************************************************************
void RandomWalk( Item* items,
                 unsigned int point,
                 unsigned int &globalMaximumValue,
                 unsigned int &globalMaximum,
                 unsigned int &iterationMaximum,
                 unsigned int numberOfSteps ) {
	
    unsigned int weight; 
    unsigned int value;
    int changedBit;

    for( int i = 0; i < numberOfSteps; i++ ) {
        weight = 0; value = 0;
        changedBit = distribution( rd ) % ELEMENTS_NUMBER;
        point = CHANGE_BIT( point, changedBit );
        SetBackpackParameters( items, point, weight, value );
        if( weight <= MAX_KNAPSACK_WEIGHT && value > globalMaximumValue ) {
            globalMaximumValue = value;
            globalMaximum = point;
            iterationMaximum = i;
        }
    }
}
//***********************************************************************************************************
void MonteCarloSearch( Item* items, 
                       unsigned int &globalMaximumValue,
                       unsigned int &globalMaximum, 
                       unsigned int &iterationMaximum,
                       unsigned int numberOfSteps ) {

    unsigned int weight;
    unsigned int value;
    int pointOfSpaceSearch;
    for( int i = 0; i < numberOfSteps; i++ ) {
        weight = 0;
        value = 0;

        pointOfSpaceSearch = distribution( rd );
        SetBackpackParameters( items, pointOfSpaceSearch, weight, value );
        if( weight <= MAX_KNAPSACK_WEIGHT && value > globalMaximumValue ) {
            globalMaximumValue = value;
            globalMaximum = pointOfSpaceSearch;
            iterationMaximum = i;
        }
    }
}
//***********************************************************************************************************
void Climbing( Item* items,
               unsigned int point,
               unsigned int &globalMaximumValue,
               unsigned int &globalMaximum,
               unsigned int &iterationMaximum,
               unsigned int numberOfSteps ) {
 
    unsigned int weight, newWeight;
    unsigned int value, newValue;
    unsigned int changedBit;
    unsigned int newPoint;

    for( int i = 0; i < numberOfSteps; i++ ) {
        weight = 0; value = 0;
        newWeight = 0; newValue = 0;
        changedBit = distribution( rd ) % ELEMENTS_NUMBER;
        newPoint = CHANGE_BIT(point, changedBit);
        
        SetBackpackParameters(items, point, weight, value);
        SetBackpackParameters(items, newPoint, newWeight, newValue);

        if(weight < MAX_KNAPSACK_WEIGHT && value > globalMaximumValue) {
            globalMaximum = point = newPoint;
            globalMaximumValue = newValue;
            iterationMaximum = i;
        } else { 
            if(weight > MAX_KNAPSACK_WEIGHT && newWeight < weight) {
                point = newPoint;
                if(newWeight <= MAX_KNAPSACK_WEIGHT) {
                    globalMaximum = point = newPoint;
                    globalMaximumValue = newValue;
                    iterationMaximum = i;
                }
            }
        }
    }
}
//***********************************************************************************************************
int main() { 
    Item Items[32];

    Items[0].Weight = 10; Items[0].Value = 23;
    Items[1].Weight = 7; Items[1].Value = 28;
    Items[2].Weight = 3; Items[2].Value = 23;
    Items[3].Weight = 4; Items[3].Value = 25;
    Items[4].Weight = 15; Items[4].Value = 5;
    Items[5].Weight = 11; Items[5].Value = 4;
    Items[6].Weight = 13; Items[6].Value = 9;
    Items[7].Weight = 15; Items[7].Value = 10;
    Items[8].Weight = 14; Items[8].Value = 4;
    Items[9].Weight = 15; Items[9].Value = 4;
    Items[10].Weight = 14; Items[10].Value = 10;
    Items[11].Weight = 16; Items[11].Value = 9;
    Items[12].Weight = 14; Items[12].Value = 6;
    Items[13].Weight = 12; Items[13].Value = 5;
    Items[14].Weight = 19; Items[14].Value = 5;
    Items[15].Weight = 37; Items[15].Value = 11;
    Items[16].Weight = 39; Items[16].Value = 19;
    Items[17].Weight = 32; Items[17].Value = 14;
    Items[18].Weight = 37; Items[18].Value = 19;
    Items[19].Weight = 31; Items[19].Value = 15;
    Items[20].Weight = 32; Items[20].Value = 20;
    Items[21].Weight = 38; Items[21].Value = 19;
    Items[22].Weight = 31; Items[22].Value = 11;
    Items[23].Weight = 8; Items[23].Value = 2;
    Items[24].Weight = 1; Items[24].Value = 7;
    Items[25].Weight = 6; Items[25].Value = 1;
    Items[26].Weight = 10; Items[26].Value = 6;
    Items[27].Weight = 4; Items[27].Value = 3;
    Items[28].Weight = 7; Items[28].Value = 7;
    Items[29].Weight = 3; Items[29].Value = 7;
    Items[30].Weight = 8; Items[30].Value = 4;
    Items[31].Weight = 5; Items[31].Value = 8;
//***********************************************************************************************************
    std::string strPoint;
//***********************************************************************************************************
    for(int i = 0; i < 20; i++) {
        unsigned int GlobalMaximum = 0;
        unsigned int GlobalMaximumValue = 0;

        unsigned int iterationMaximum = 0;
        strPoint = "";
        time_t timeStart;
        time_t timeOfSearch; 
        timeStart = time( NULL );

        RandomWalk(Items, 0, GlobalMaximumValue, GlobalMaximum, iterationMaximum, 100);
        /* MonteCarloSearch( Items, GlobalMaximumValue, GlobalMaximum, iterationMaximum, 1000000); */
        /* Climbing(Items, 0, GlobalMaximumValue, GlobalMaximum, iterationMaximum, 10); */
        /* FullSearch(Items, GlobalMaximumValue, GlobalMaximum); */ 
         
        timeOfSearch = time( NULL ) - timeStart;
    //***********************************************************************************************************
        PointAsStr( GlobalMaximum, strPoint );

        unsigned int weight = 0;
        unsigned int value = 0;
        SetBackpackParameters( Items, GlobalMaximum, weight, value );

        std::cout << strPoint << std::endl;
        std::cout << "Waga: "<< weight << std::endl;
        std::cout << "Wartosc: "<< value << std::endl;
        std::cout << "Iteration maximum: "<< iterationMaximum + 1 << std::endl;
        std::cout << "Czas: "<< timeOfSearch << std::endl << std::endl;
    }
    return 0;
}
