#include <math.h>
#include <random>
#include <iostream>

std::random_device rd;
std::uniform_real_distribution<double> distribution(-1, 1);

void evalPi( int iter ) {
    int pointsInCircle = 0;
    double x, y;
	for( int i = 0; i < iter; i++ ) {
            x = distribution(rd); y = distribution(rd);
            if( x * x + y * y <= 1 ) { pointsInCircle++; }
	}
	double pi = 4. * pointsInCircle / iter;

	std::cout << "L. losowan: " << iter << std::endl
                  << "L. pkt w kole " << pointsInCircle << std::endl
                  << "L. Pi: " << pi << std::endl << std::endl;
}

int main() {
    for( int i = 1; i < 100000000; i = i * 10 ) {
        evalPi(i);
    }

    return 0;
}

