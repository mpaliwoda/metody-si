#include <iostream>
#include <math.h>
#include <random>
#include <string>
#include <limits.h>

using namespace std;

#define GENOTYPE_TYPE unsigned int

#define MAX_KNAPSACK_WEIGHT 200u //MAKS POJEMNOSC PLECAKA
#define ELEMENTS_NUMBER ( sizeof( int ) * 8 ) //ILOSC ELEM W PLECAKU

#define POPULATION_NUMBER 40 //ROZMIAR POPULACJI
#define MAX_GENERATION_NUMBER 12 //ILOSC GENERACJI
#define PENALTY_FUNCTION true //CZY ZASTOSOWAC FUNKCJE KARY

//GENERATOR LICZB LOSOWYCH
random_device rd;
uniform_int_distribution<int> distribution(INT_MIN, INT_MAX );

//***********************************************************************************************************
struct Item {
    unsigned short Weight;
    unsigned short Value;
};

struct Individual {
    GENOTYPE_TYPE Genotype;

    //Adaptation func result
    int AFResult;
    unsigned short Weight;
    unsigned short Value;

    Individual(Individual &ind):
        Genotype(ind.Genotype), AFResult(ind.AFResult),
        Weight(ind.Weight), Value(ind.Value) {}

    Individual(): 
        Genotype(0), AFResult(0),
        Weight(0), Value(0) {}

    ~Individual() {}
};
#define CHECK_BIT(var, pos)                     ((var) & (1 << (pos))) //CZY BIT JEST ZAJETY
#define CHANGE_BIT(var, pos)                    ((var) ^ (1 << (pos))) //ZMIEN WARTOSC W PODANYM BICIE
#define SET_BIT(var, pos, val)                  if(val) var |= (1 << pos); else var &= ~(1 << pos) //USTAW WARTOSC NA PODANEJ POZYCJI
#define REPLACE_BIT(var1, var2, pos, buff)      buff = CHECK_BIT(var2, pos);\
                                                SET_BIT(var2, pos, CHECK_BIT(var1, pos));\
                                                SET_BIT(var1, pos, buff)

#define PenaltyFunction(weight, value, result)  result= \
                                                value - (weight - MAX_KNAPSACK_WEIGHT) \
                                                * (weight - MAX_KNAPSACK_WEIGHT)
//***********************************************************************************************************
void AdaptationFunction(Individual &individual, Item *items, bool penaltyFunction) {
    individual.Value = 0;
    individual.Weight = 0;

    for(int i = 0; i < ELEMENTS_NUMBER; i++) {
        if(CHECK_BIT(individual.Genotype, i)) {
            individual.Weight += items[i].Weight;
            individual.Value += items[i].Value;
        }
    }
    
    if(individual.Weight <= MAX_KNAPSACK_WEIGHT) 
        individual.AFResult = individual.Value;
    else {
        if(penaltyFunction)
            PenaltyFunction(individual.Weight, individual.Value, individual.AFResult);
        else 
            individual.AFResult = 0;
    }
}
//***********************************************************************************************************
void GenerateStartingPopulation(Individual *individual, Item *items, bool penaltyFunction) {
    for(int i = 0; i < POPULATION_NUMBER; i++) {
        do {
            individual[i].Genotype = distribution(rd);
            AdaptationFunction(individual[i], items, penaltyFunction);
        } while(individual[i].AFResult >= 0);
    }
}
//***********************************************************************************************************
void TournamentSelection(
        Individual *population, 
        Individual *auxiliaryPopulation,
        int tournamentSize) {

    unsigned int randomNumber = 0;
    for(int i = 0; i < POPULATION_NUMBER; i++) {
        randomNumber = distribution(rd);
        randomNumber %= POPULATION_NUMBER;

        auxiliaryPopulation[i] = population[randomNumber];

        for(int j = 0; j < tournamentSize; j++) {
            randomNumber = distribution(rd);
            randomNumber %= POPULATION_NUMBER;

            if(population[randomNumber].AFResult > auxiliaryPopulation[randomNumber].AFResult)
                auxiliaryPopulation[i] = population[randomNumber];
        }
    }
}
//***********************************************************************************************************
void TwoPointsCut(Individual &p1, Individual &p2) {
    int cutPoint1 = distribution(rd);
    cutPoint1 %= (ELEMENTS_NUMBER + 1);

    if(cutPoint1 == ELEMENTS_NUMBER) return;

    bool buff = false;

    if(cutPoint1 == ELEMENTS_NUMBER - 1) {
        REPLACE_BIT(
            p1.Genotype, 
            p2.Genotype, 
            ELEMENTS_NUMBER - 1,
            buff
        );
    } else {
        int cutPoint2 = distribution(rd); 
        cutPoint2 %= (ELEMENTS_NUMBER - cutPoint1);
        cutPoint2 += cutPoint1 + 1;

        for(int i = cutPoint1; i < cutPoint2; i++) {
            REPLACE_BIT(
                p1.Genotype, 
                p2.Genotype, 
                i,
                buff
            );
        }
    }
}
//***********************************************************************************************************
void CrossIndividuals(
            Individual* population,
            unsigned int probability,
            Item* items,
            bool penaltyFunction) {

    unsigned int randomNumber = 0;
    for(int i = 0; i < POPULATION_NUMBER - 1; i++) {
        randomNumber = distribution(rd);
        randomNumber = (randomNumber % 100) + 1;

        if(randomNumber <= probability) {
            TwoPointsCut(population[i], population[i+1]);
            AdaptationFunction(population[i], items, penaltyFunction);
            AdaptationFunction(population[i+1], items, penaltyFunction);
        }
    }
}
//***********************************************************************************************************
void MutationIndividuals(
            Individual* population,
            unsigned int probability,
            Item* items,
            bool penaltyFunction) {

    unsigned int randomNumber = 0;
    bool mutated = false;
    for(int i = 0; i < POPULATION_NUMBER; i++) {
        mutated = false;
        for(int j = 0; j < ELEMENTS_NUMBER; j++) {
            randomNumber = distribution(rd);
            randomNumber = (randomNumber % 100) + 1;

            if(randomNumber <= probability) {
                population[i].Genotype = CHANGE_BIT(population[i].Genotype, j);
                mutated = true;
            }
        }
        
        if(mutated) {
            AdaptationFunction(population[i], items, penaltyFunction);
        }
    }
}
//***********************************************************************************************************
void NumOfGens(
        Individual* population, 
        int* results) {
        
    for(int i = 0; i < POPULATION_NUMBER; i++) {
            for(int j = 0; j < ELEMENTS_NUMBER; j++) {
                if(CHECK_BIT(population[i].Genotype, j)) {
                    results[j]++;
                }
        }
    }
}
//***********************************************************************************************************
void PrintSchematics(int* schematics) {
    for(int i = 0; i < ELEMENTS_NUMBER; i++) {
        cout << "g" << i + 1 << ": " << schematics[i] << endl;
    }
    cout << endl << endl;
}
//***********************************************************************************************************
void PointAsStr( unsigned int point, string &strPoint ) {
    for( int i = 0; i < ELEMENTS_NUMBER; i++ ) {
        if( CHECK_BIT( point, i ) ) {
            strPoint += '1'; 
        } else {
            strPoint += '0';
        }
    }
}
//**********************************************************************************************************
void GetBestIndividual(
        Individual* population,
        int &bestIndividual) {
    
    bestIndividual = 0;
    for(int i = 1; i < POPULATION_NUMBER; i++) {
        if(population[i].AFResult > population[bestIndividual].AFResult) {
           bestIndividual = i; 
        }
    }
}
//**********************************************************************************************************
int main() { 
    Item Items[32];

    Items[0].Weight = 10; Items[0].Value = 23;
    Items[1].Weight = 7; Items[1].Value = 28;
    Items[2].Weight = 3; Items[2].Value = 23;
    Items[3].Weight = 4; Items[3].Value = 25;
    Items[4].Weight = 15; Items[4].Value = 5;
    Items[5].Weight = 11; Items[5].Value = 4;
    Items[6].Weight = 13; Items[6].Value = 9;
    Items[7].Weight = 15; Items[7].Value = 10;
    Items[8].Weight = 14; Items[8].Value = 4;
    Items[9].Weight = 15; Items[9].Value = 4;
    Items[10].Weight = 14; Items[10].Value = 10;
    Items[11].Weight = 16; Items[11].Value = 9;
    Items[12].Weight = 14; Items[12].Value = 6;
    Items[13].Weight = 12; Items[13].Value = 5;
    Items[14].Weight = 19; Items[14].Value = 5;
    Items[15].Weight = 37; Items[15].Value = 11;
    Items[16].Weight = 39; Items[16].Value = 19;
    Items[17].Weight = 32; Items[17].Value = 14;
    Items[18].Weight = 37; Items[18].Value = 19;
    Items[19].Weight = 31; Items[19].Value = 15;
    Items[20].Weight = 32; Items[20].Value = 20;
    Items[21].Weight = 38; Items[21].Value = 19;
    Items[22].Weight = 31; Items[22].Value = 11;
    Items[23].Weight = 8; Items[23].Value = 2;
    Items[24].Weight = 1; Items[24].Value = 7;
    Items[25].Weight = 6; Items[25].Value = 1;
    Items[26].Weight = 10; Items[26].Value = 6;
    Items[27].Weight = 4; Items[27].Value = 3;
    Items[28].Weight = 7; Items[28].Value = 7;
    Items[29].Weight = 3; Items[29].Value = 7;
    Items[30].Weight = 8; Items[30].Value = 4;
    Items[31].Weight = 5; Items[31].Value = 8;
//***********************************************************************************************************
    uniform_int_distribution<int> distribution(INT_MIN, INT_MAX);
    //POPLUACJA GLOWNA
    Individual Population[POPULATION_NUMBER];

    //POMOCNICZA POPULACJA POTRZEBNA DO REPRODUKCJI
    Individual BuffPopulation[POPULATION_NUMBER];
    
    //POWTARZALNOSC GENOW (SCHEMATY)
    int Schematics[ELEMENTS_NUMBER] = {0};
    
    //Nr najlepszego osobnika w populacji
    int BestIndividual;

    GenerateStartingPopulation(Population, Items, PENALTY_FUNCTION);
    GetBestIndividual(Population, BestIndividual);
    cout << "Generation: " << 1 << endl;
    NumOfGens(Population, Schematics);
    PrintSchematics(Schematics);
    cout << "Best ind.: " << endl;
    string strGenotype;
    PointAsStr(Population[BestIndividual].Genotype, strGenotype);
    cout << strGenotype << endl;
    cout << "Value = " << Population[BestIndividual].Value << endl;
    cout << "Weight = " << Population[BestIndividual].Weight<< endl;

    for(int i = 2; i <= MAX_GENERATION_NUMBER; i++) {
        TournamentSelection(Population, BuffPopulation, 2);
        CrossIndividuals(BuffPopulation, 100, Items, PENALTY_FUNCTION);
        MutationIndividuals(BuffPopulation, 1, Items, PENALTY_FUNCTION);
        GetBestIndividual(BuffPopulation, BestIndividual);

        cout << "Generation: " << i << endl;
        NumOfGens(Population, Schematics);
        PrintSchematics(Schematics);
        cout << "Best ind.: " << endl;
        strGenotype = "";
        PointAsStr(Population[BestIndividual].Genotype, strGenotype);
        cout << strGenotype << endl;
        cout << "Value = " << Population[BestIndividual].Value << endl;
        cout << "Weight = " << Population[BestIndividual].Weight<< endl;

        for(int j = 0; j < POPULATION_NUMBER; j++) {
            Population[j] = BuffPopulation[j];
        }
    }
//***********************************************************************************************************

    return 0;
}
